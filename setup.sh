#!/usr/bin/sh

sudo apt-get update
sudo apt-get install -y python-nltk python-numpy

echo "
import nltk

a = nltk.downloader.Downloader(\"http://nltk.github.com/nltk_data\",\"/usr/share/nltk_data\")
a.download('all')
" | sudo python
